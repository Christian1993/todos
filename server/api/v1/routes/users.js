const express = require('express');
const router = express.Router();

const User = require('../models/User');

router.post('/', (req, res) => {
  const {
    name,
    email,
    password
  } = req.body;
  User.findOne({
      email
    })
    .then(user => {
      if (user) {
        return res.status(400).json({
          msg: "Invalid Credentials"
        })
      } else {
        const newUser = {
          name,
          email,
          password
        }
      }
      newUser.save();
      return res.status(201).json({
        msg: "Created"
      })
    })

})

module.exports = router;
