const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
  title : {
    type: String,
    required: [true, 'Title is required']
  },
  done: {
    type: Boolean,
    default: false
  }
})

const Todo = mongoose.model ('todo', TodoSchema);
module.exports = Todo;
