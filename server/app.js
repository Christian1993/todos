const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const pagesRouter = require('./api/v1/routes/pages');
const todosRouter = require('./api/v1/routes/todos');
const userRouter = require('./api/v1/routes/users');

const app = express();
const db = config.get('mongoUrl');
app.use(express.json());

mongoose.connect(db, {
    useNewUrlParser: true
  })
  .then(() => console.log('Connection established'))
  .catch(err => console.log(err));

app.use(express.static(__dirname + '/../client/build'));

app.use(pagesRouter);
app.use('/api/v1/todos', todosRouter);

module.exports = app;
